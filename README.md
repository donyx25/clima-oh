# Reto tecnico - Clima Oh

# Aplicacion del clima

# El reto

Desarrollar una aplicacion que muestre el clima de lima, asi mismo podemos buscar cualquier ciudad y consultar el clima correspondiente.

## _Navigation APP_
En la aplicación se encuentra las siguientes pantallas.
- [OnboardingView] - Se muestra una guia de las funcionalidades de la aplicacion la cuenta con 3 pasos.
- [WeatherView] - Se muestra el clima de Lima por defecto, con el detalle correspondiente.
- [SearchView] - Se puede buscar una ciudad a consultar, asi mismo mostrara el detalle del clima.

## Architecture patterns
- [MVVM]
Se utilizó la arquitectura MVVM , para dividir en un número pequeño de responsabilidades que tiene la aplicación las cuales deben estar bien definidas.
De tal manera que el código sea más fácil de entender, modificar y mantener.

## Librerías externas

- [Alamofire] 
En esta oportunidad no se decidio utilizar esta libreria ya que se utilizo las funcionalidades nativas brindadas por el propio Apple, usando URLSession

## Otros
- [SwiftUI]
Se utilizó Swiftui para el desarrollo del challenge, en el que se aplica el uso de observadores para manejar los datos reactivos que se presentan en la pantalla de usuario.

- [https://openweathermap.org/]
Se utilizo la siguiente api para consultar el estado del clima de cada ciudad seleccionada.


## License

**Donnadony Mollo Quicaño**
