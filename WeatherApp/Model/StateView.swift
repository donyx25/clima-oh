//
//  StateView.swift
//  WeatherApp
//
//  Created by Donnadony Mollo on 13/09/22.
//

import Foundation

enum StateView {
    case loading
    case success
    case failed
}

