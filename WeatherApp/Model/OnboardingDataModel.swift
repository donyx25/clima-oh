//
//  OnboardingDataModel.swift
//  WeatherApp
//
//  Created by Donnadony Mollo on 12/09/22.
//

import Foundation
import SwiftUI

struct OnboardingDataModel {
    
    var image: String
    var title: String
    var color: Color?
}

extension OnboardingDataModel {
    static var data: [OnboardingDataModel] = [
        OnboardingDataModel(image: "nubeysol", title: "Una aplicación que indica el tiempo según la ubicación."),
        OnboardingDataModel(image: "arcoiris", title: "Revisa el clima segun la ciudad que se busca."),
        OnboardingDataModel(image: "truenoysol", title: "Los pronosticos del clima son los mas actualizados."),
        
    ]
}


