//
//  Coordinate.swift
//  WeatherApp
//
//  Created by Donnadony Mollo on 13/09/22.
//

import Foundation

struct Coordinate: Codable {
    let lon, lat: Double
    
    static func emptyInit() -> Coordinate {
        return Coordinate(lon: 0, lat: 0)
    }
}

