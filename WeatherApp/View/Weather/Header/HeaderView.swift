//
//  HeaderView.swift
//  WeatherApp
//
//  Created by Donnadony Mollo on 13/09/22.
//

import SwiftUI

struct HeaderView: View {
    let data: CurrentWeather
    let showSearch: Bool
    @EnvironmentObject var wVM: WeatherViewModel

    var weatherName: String {
        var result = ""
        if let weather = data.elements.first {
            result = weather.main
        }
        return result
    }
    
    var temperature: String {
        return "\(Int(data.mainValue.temp))°"
    }

    var body: some View {
        VStack {
            
            
            if showSearch {
                HStack{
                    Spacer(minLength: 0)
                    
                    Button {
                        withAnimation(.easeInOut){
                            wVM.menu = .search
                        }
                    } label: {
                        Image(systemName: "magnifyingglass")
                            .resizable()
                            .frame(width: 20, height: 20, alignment: .center)
                            .foregroundColor(.secondary)
                    }
                    .padding(.trailing,20)

                }
            }
            
            Text(data.name)
                .font(.largeTitle)
                .fontWeight(.medium)
                .foregroundColor(.secondary)
            
            Text(weatherName)
                .font(.body)
                .fontWeight(.light)
                .padding(.bottom, 4)
                .foregroundColor(.secondary)
            Text(temperature)
                .font(.system(size: 86))
                .fontWeight(.thin)
                .foregroundColor(.secondary)
        }
        .padding(.vertical, 24)
    }
}
