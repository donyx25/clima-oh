//
//  HourCellView.swift
//  WeatherApp
//
//  Created by Donnadony Mollo on 13/09/22.
//

import SwiftUI

struct HourCellView: View {
    var data: ForecastWeather

    var hour: String {
        return data.date.dateFromMilliseconds().hour()
    }

    var temperature: String {
        return "\(Int(data.mainValue.temp))°"
    }

    var icon: String {
        var image = "WeatherIcon"
        if let weather = data.elements.first {
            image = weather.icon
        }
        return image
    }

    var body: some View {
        VStack {
            Text(hour)
                .foregroundColor(.secondary)
            Text("\(data.mainValue.humidity)%")
                .font(.system(size: 12))
                .foregroundColor(.secondary)
            Image(icon)
                .resizable()
                .aspectRatio(UIImage(named: icon)!.size, contentMode: .fit)
                .frame(width: 30, height: 30)
            
            Text(temperature)
                .foregroundColor(.secondary)
        }.padding(.all, 0)
    }
}
