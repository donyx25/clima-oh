//
//  HourView.swift
//  WeatherApp
//
//  Created by Donnadony Mollo on 13/09/22.
//


import SwiftUI

struct HourView: View {
    let data: [ForecastWeather]
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack {
                ForEach(data) { data in
                    HourCellView(data: data)
                    Spacer().frame(width: 24)
                }
            }.padding(.horizontal, 24)
        }
    }
}
