//
//  WeatherView.swift
//  WeatherApp
//
//  Created by Donnadony Mollo on 12/09/22.
//

import SwiftUI

struct WeatherView: View {
    
    
    @EnvironmentObject var wVM: WeatherViewModel
    
    @State private var alert: Alert?
    @State var showAlert = false
    
    var body: some View {
        
        VStack(spacing:0){
            if wVM.stateView  == .loading {
                ActivityIndicatorView(isAnimating: true).configure {
                    $0.color = .secondary
                }
            }
            
            
            if wVM.stateView  == .success {
                
                HeaderView(data: wVM.currentWeather,showSearch: true)
                    .environmentObject(wVM)
                Spacer()
                
                ScrollView(.vertical, showsIndicators: false) {
                    VStack {
                        
                        Text(wVM.currentDescription)
                            .foregroundColor(.secondary)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .padding( [.leading,.trailing],
                                24
                            )
                        Divider()
                        // This is today
                        DayCellView(data: wVM.todayWeather)
                        Divider()
                        HourView(data: wVM.hourlyWeathers)
                        Divider()

                        DayView(data: wVM.dailyWeathers)
                        Divider()

                        

                        DetailView(data: wVM.currentWeather)
                        Divider()
                       

                    }
                }
                Spacer()
            }
            
            if wVM.stateView == .failed {
                Button(action: {
                    self.wVM.retry()
                }) {
                    Text("Please try again, ocurred an error!.")
                        .foregroundColor(.white)
                }
            }
            
            
        }
        .onAppear{
            print(wVM.stateView.hashValue)
            wVM.getData()
        }
        
    }
}
