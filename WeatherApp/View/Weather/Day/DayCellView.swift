//
//  DayCellView.swift
//  WeatherApp
//
//  Created by Donnadony Mollo on 13/09/22.
//

import SwiftUI

struct DayCellView: View {
    let data: ForecastWeather
    
    var day: String {
        return data.date.dateFromMilliseconds().dayWord()
    }
    var temperatureMax: String {
        return "\(Int(data.mainValue.tempMax))°"
    }

    var temperatureMin: String {
        return "\(Int(data.mainValue.tempMin))°"
    }
    
    var icon: String {
        var image = "WeatherIcon"
        if let weather = data.elements.first {
            image = weather.icon
        }
        return image
    }

    var body: some View {
        HStack {
            Text(day)
                .foregroundColor(.secondary)
                .frame(width: 150, alignment: .leading)

            Image(icon)
                .resizable()
                .aspectRatio(UIImage(named: icon)!.size, contentMode: .fit)
                .frame(width: 30, height: 30)

            Spacer()
            Text(temperatureMax)
                .foregroundColor(.secondary)
            Spacer().frame(width: 34)
            Text(temperatureMin)
                .foregroundColor(.secondary)
        }.padding(.horizontal, 24)
    }
}
