//
//  DayView.swift
//  WeatherApp
//
//  Created by Donnadony Mollo on 13/09/22.
//
import SwiftUI

struct DayView: View {
    let data: [ForecastWeather]
    
    var body: some View {
        VStack {
            ForEach(data, id: \.date) { data in
                DayCellView(data: data)
            }
        }
    }
}
