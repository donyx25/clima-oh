//
//  SearchCityView.swift
//  WeatherApp
//
//  Created by Donnadony Mollo on 12/09/22.
//

import SwiftUI

struct SearchCityView: View {
    
    
    @EnvironmentObject var wVM: WeatherViewModel
    @State var searchText: String = ""
    
    var body: some View {
        
      
        
        
        NavigationView{
            VStack{
                
                
                if wVM.stateView == .loading {
                    ActivityIndicatorView(isAnimating: true).configure {
                        $0.color = .secondary
                    }
                }
                
                if wVM.stateView  == .success {
                    
                    if !wVM.cityWeather.name.isEmpty {
                        List{
                            NavigationLink {
                                SearchDetailView(cityID: "\(wVM.cityWeather.id)")
                            } label: {
                                SearchCellView(data: wVM.cityWeather)
                            }

                        }
                    }
                    else{
                        Text("No information for that name city.")
                            .foregroundColor(.white)
                    }
                    
                }
                if wVM.stateView == .failed {
                    Text("No information for that name city.")
                        .foregroundColor(.white)
                }
                
                
            }
            .navigationBarItems(leading: HStack{
                Button {
                    withAnimation(.easeInOut){
                        wVM.menu = .main
                    }
                } label: {
                    Text("Back")
                }

            })
            .onChange(of: searchText, perform: { newValue in
                
                if wVM.stateView != .loading {
                    wVM.getCities(q: searchText)
                }
                
                
            })
            .searchable(text: $searchText, placement: .navigationBarDrawer(displayMode: .always))
            .disableAutocorrection(true)
            
            .navigationTitle("Cities")
            .navigationBarTitleDisplayMode(.inline)
        }
        
    }
}

struct SearchCityView_Previews: PreviewProvider {
    static var previews: some View {
        SearchCityView()
    }
}
