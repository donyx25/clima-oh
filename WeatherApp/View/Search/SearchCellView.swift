//
//  SearchCellView.swift
//  WeatherApp
//
//  Created by Donnadony Mollo on 13/09/22.
//

import SwiftUI

struct SearchCellView: View {
    let data: CurrentWeather

    var weatherName: String {
        var result = ""
        if let weather = data.elements.first {
            result = weather.main
        }
        return result
    }
    
    var temperature: String {
        return "\(Int(data.mainValue.temp))°"
    }

    var body: some View {
        VStack {
            
            
            HStack(alignment: .center) {
                Text(data.name)
                    .font(.title)
                    .fontWeight(.medium)
                    .foregroundColor(.secondary)
                
                Spacer(minLength: 0)
                
                VStack{
                    Text(weatherName)
                        .font(.body)
                        .fontWeight(.light)
                        .padding(.bottom, 4)
                        .foregroundColor(.secondary)
                    Text(temperature)
                        .font(.body)
                        .fontWeight(.thin)
                        .foregroundColor(.secondary)
                }
            }
            
            
           
        }
        .padding(.vertical, 24)
    }
}

