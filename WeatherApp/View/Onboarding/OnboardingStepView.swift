//
//  OnboardingStepView.swift
//  WeatherApp
//
//  Created by Donnadony Mollo on 12/09/22.
//

import SwiftUI

struct OnboardingStepView: View {
    
    var data: OnboardingDataModel
    
    var body: some View {
        VStack {
            
            
            Image(data.image)
                .resizable()
                .scaledToFit()
                .frame(width: 200, height: 200, alignment: .center)
                //.padding(.bottom, 50)
            
            Spacer()
                .frame(height: 40)
            Text(data.title)
                .font(.system(size: 20, design: .rounded))
                .foregroundColor(.secondary)
                
                .padding(.bottom, 20)
                .multilineTextAlignment(.center)
            
        }
        .padding()
        .contentShape(Rectangle())
    }
    
    
}
