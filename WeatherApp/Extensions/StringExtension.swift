//
//  StringExtension.swift
//  WeatherApp
//
//  Created by Donnadony Mollo on 13/09/22.
//

import Foundation


extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
