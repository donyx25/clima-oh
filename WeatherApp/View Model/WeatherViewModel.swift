//
//  WeatherViewModel.swift
//  WeatherApp
//
//  Created by Donnadony Mollo on 12/09/22.
//

import Foundation



class WeatherViewModel:  ObservableObject{
    
    let client = OpenweatherAPIClient()
    
    @Published var menu : MenuWeather = .main
    
    var stateView: StateView = StateView.loading {
        willSet {
            objectWillChange.send()
        }
    }

    var currentWeather = CurrentWeather.emptyInit() {
        willSet {
            objectWillChange.send()
        }
    }
    
    var cityWeather = CurrentWeather.emptyInit() {
        willSet {
            objectWillChange.send()
        }
    }
    
    var cityTodayWeather = ForecastWeather.emptyInit() {
        willSet {
            objectWillChange.send()
        }
    }
    
    var cityCurrentDescription = "" {
        willSet {
            objectWillChange.send()
        }
    }
    
    var todayWeather = ForecastWeather.emptyInit() {
        willSet {
            objectWillChange.send()
        }
    }

    var hourlyWeathers: [ForecastWeather] = [] {
        willSet {
            objectWillChange.send()
        }
    }

    var dailyWeathers: [ForecastWeather] = [] {
        willSet {
            objectWillChange.send()
        }
    }
    
    var currentDescription = "" {
        willSet {
            objectWillChange.send()
        }
    }
    
    private var stateCurrentWeather = StateView.loading
    private var stateForecastWeather = StateView.loading
    
    // Lima
    var cityId = "3936456"

    init() {
        //getData()
    }
    
    func retry() {
        stateView = .loading
        stateCurrentWeather = .loading
        stateForecastWeather = .loading
        
        getData()
    }
    
    
    func getCities(q city: String){
        client.getCities(at: city) { [weak self] currentWeather, error in
            guard let ws = self else { return }
            if let currentWeather = currentWeather {
                ws.cityWeather = currentWeather
                ws.cityTodayWeather = currentWeather.getForecastWeather()
                ws.cityCurrentDescription = currentWeather.description()
                print("getCurrentWeather \(ws.cityWeather)")
                ws.stateCurrentWeather = .success
            } else {
                ws.cityWeather = CurrentWeather.emptyInit()
                ws.stateCurrentWeather = .failed
            }
            ws.updateStateView()
        }
    }
    
    func getData() {
        client.getCurrentWeather(at: cityId) { [weak self] currentWeather, error in
            guard let ws = self else { return }
            if let currentWeather = currentWeather {
                ws.currentWeather = currentWeather
                ws.todayWeather = currentWeather.getForecastWeather()
                ws.currentDescription = currentWeather.description()
                print("getCurrentWeather \(ws.cityId)")
                ws.stateCurrentWeather = .success
            } else {
                ws.stateCurrentWeather = .failed
            }
            ws.updateStateView()
        }

        client.getForecastWeather(at: cityId) { [weak self] forecastWeatherResponse, error in
            guard let ws = self else { return }
            if let forecastWeatherResponse = forecastWeatherResponse {
                ws.hourlyWeathers = forecastWeatherResponse.list
                ws.dailyWeathers = forecastWeatherResponse.dailyList
                ws.stateForecastWeather = .success
            } else {
                ws.stateForecastWeather = .failed
            }
            ws.updateStateView()
        }
    }
        
    private func updateStateView() {
        if stateCurrentWeather == .success, stateForecastWeather == .success {
            stateView = .success
        }
        
        if stateCurrentWeather == .failed, stateForecastWeather == .failed {
            stateView = .failed
        }
    }
    
    
}



enum MenuWeather{
    case main,search
}
