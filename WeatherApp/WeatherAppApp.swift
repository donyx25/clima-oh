//
//  WeatherAppApp.swift
//  WeatherApp
//
//  Created by Donnadony Mollo on 12/09/22.
//

import SwiftUI

@main
struct WeatherAppApp: App {
    
    var body: some Scene {
        WindowGroup {
           ContentView()
        }
    }
}
