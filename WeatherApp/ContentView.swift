//
//  ContentView.swift
//  WeatherApp
//
//  Created by Donnadony Mollo on 12/09/22.
//

import SwiftUI

struct ContentView: View {
    
    @State private var onboardinDone = false
    var data = OnboardingDataModel.data
    @ObservedObject var wVM = WeatherViewModel()
    
    
    var body: some View {
        
        
        Group {
            if !onboardinDone {
                OnboardingView(data: data, doneFunction: {
                    /// Update your state here
                    self.onboardinDone = true
                    print("done onboarding")
                })
            } else {
                
                
                switch wVM.menu{
                case .main:
                    WeatherView()
                        .environmentObject(wVM)
                case .search:
                    SearchCityView()
                        .environmentObject(wVM)
                }
                
                
            }
        }
        
        
    }
}
